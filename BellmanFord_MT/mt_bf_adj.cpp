/*
*
*	Ali Eker
*	Parallel implementation of Bellman Ford single source shortest path algorithm
*	07/29/2019
*
*	Directed Graph
* 	All edge weights are 1
*   ./mt_bf 256 2.1 1
*/

#include <iostream>
#include <cstdlib>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <bits/stdc++.h>
#include <pthread.h>
#include <math.h>

// TO-DO 1: Support for not even partitions

/*
struct vertex_data
{
    int dist;
    int padding[15];
} __attribute__((packed));
*/

#define MAX 100000
#define THREADS 64 // Number of threads for building the graph

int ** adj_list;
int * vertex_list;
int * degree_list;

int N;
int NCPUS;
bool relaxed_global = false;
double sec = 0;
double probability; // Skewness degree 

pthread_mutex_t sec_lock; 
pthread_barrier_t barrier;

inline void * construct (void * id)
{
	std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now(); 

    int tid = (intptr_t) id;
    int partition_size = N / THREADS;
	int range_begin = tid * partition_size;
	int range_end = tid * partition_size + partition_size - 1;
    unsigned int seed = tid;
 
	for (int i = range_begin; i <= range_end; i++) vertex_list[i] = MAX; 
    for (int i = range_begin; i <= range_end; i++) adj_list[i] = new int[N];
 
    double beta = pow(N, probability);
    int x, target;

    for (int i = range_begin; i <= range_end; i++)
    { 
        x = ((int) rand_r(&seed)) % N;
        
        if (x == 0) degree_list[i] = 0;
        else degree_list[i] = (int) floor(beta / pow(x, probability));
    }
    
    for (int i = range_begin; i <= range_end; i++)
	{
		for (int k = 0; k < degree_list[i]; k++)
        {
	        target = ((int) rand_r(&seed)) % N;

            if (target == i)
            {
                k--;
                continue;
            }

            adj_list[i][target] = 1;

            //if (adj_list[i][target] == 0) adj_list[i][target] = 1;
            //else
            //{
            //    k--;
            //    continue;
            //}
        }
    }

    std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	
	pthread_mutex_lock(&sec_lock);	
	if (sec_local > sec) sec = sec_local;	
	pthread_mutex_unlock(&sec_lock);	

    pthread_exit(NULL);
}

inline void * run (void * id) 
{
	int tid = (intptr_t) id;
	int partition_size = N / NCPUS; 
	int range_begin = tid * partition_size;
	int range_end = tid * partition_size + partition_size - 1;
    bool relaxed;
    int new_dist;

    // CPU Affinity
    /*
    pthread_t thread = pthread_self();
    cpu_set_t cpuMask;

    CPU_ZERO(&cpuMask);
    CPU_SET(tid, &cpuMask);

    if (CPU_ISSET(tid, &cpuMask) >= 0)
    {
       int s = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask); 
       if (s != 0) perror("\nCPU Affinity Broken\n\n");
    }
    else perror("\nCPU Affinity Broken\n\n");
    */
    
	std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now();

	for (int i = 0; i < N - 1; i++)
	{
        relaxed = false;
        if (tid == 0) relaxed_global = false;
        
		for (int j = range_begin; j <= range_end; j++)
		{
			for (int k = 0; k < N; k++)
			{
				if (adj_list[j][k] > 0) // if vertex j has an out edge to vertex k
				{
					new_dist = vertex_list[j] + adj_list[j][k];

					if (new_dist < vertex_list[k]) 
                    {
                        relaxed = true;
                        vertex_list[k] = new_dist;		
                    }
				}
			}
		}	

	    pthread_mutex_lock(&sec_lock);	
        relaxed_global = relaxed_global | relaxed;
	    pthread_mutex_unlock(&sec_lock);	
		pthread_barrier_wait(&barrier);			

        if (!relaxed_global) break;
		pthread_barrier_wait(&barrier);			
	}

	std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	
	pthread_mutex_lock(&sec_lock);	
	if (sec_local > sec) sec = sec_local;	
	pthread_mutex_unlock(&sec_lock);	

    pthread_exit(NULL);
}

int main (int argc, char ** argv)
{
	if (argc != 4) assert(0);

	int num_vertices = atoi(argv[1]); // Should be power of 32 -> 1024, 8192, 131072 (128K), 262144 (256K)
	N = num_vertices;

	probability = atof(argv[2]); // Power law skewness
    // srand(time(NULL));

	int ncpus = atoi(argv[3]);
	pthread_t * threads = new pthread_t [ncpus];
	pthread_t * build_threads = new pthread_t [THREADS];
	NCPUS = ncpus;
	
	pthread_attr_t attr;
	void * status;
	int rc = pthread_mutex_init(&sec_lock, NULL); if (rc) assert(0);
	rc = pthread_barrier_init(&barrier, NULL, THREADS); if (rc) assert(0);
	
	std::cout << "\nConstructing the graph ...\n";
	
	vertex_list = new int [num_vertices];
	adj_list = new int * [num_vertices];	
	degree_list = new int [num_vertices];

    pthread_attr_init(&attr);
   	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);	
	
	for (int i = 0; i < THREADS; i++) 
	{
      		rc = pthread_create(&threads[i], &attr, construct, (void *) (intptr_t) i);
      		if (rc) assert(0);
    }
   	
	for (int i = 0; i < THREADS; i++) 
	{
     		rc = pthread_join(threads[i], &status);
      		if (rc) assert(0);
    }

	vertex_list[0] = 0; // Vertex 0 is the source
	printf("Constructing the graph is done. Took %.5f seconds.\n\n", sec / 1000000);

    /*
    int * arr = new int [N];
	std::cout << "Degree Histogram: \n";
	for (int i = 0; i < N; i++) 
    {
        for (int j = 0; j < N; j++) if (adj_list[i][j]) arr[i]++; 
    }

    int * degree = new int [N];
	for (int i = 0; i < N; i++) degree[arr[i]]++;
	for (int i = 0; i < N; i++) printf("%d ", degree[i]); 
	std::cout << "\n\n"; 

    delete [] arr;
	delete [] degree;
    */

    delete [] degree_list;
    sec = 0;
	std::cout << "Running the Bellman-Ford for sssp ...\n";
	
	pthread_attr_init(&attr);
   	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);	
	
	for (int i = 0; i < ncpus; i++) 
	{
      		rc = pthread_create(&threads[i], &attr, run, (void *) (intptr_t) i);
      		if (rc) assert(0);
    }
   	
	for (int i = 0; i < ncpus; i++) 
	{
     		rc = pthread_join(threads[i], &status);
      		if (rc) assert(0);
    }

	printf("Sssp is done. Took %.5f seconds.\n\n", sec / 1000000);

	std::cout << "Vertex List: \n";
	for (int i = 0; i < 32 /*num_vertices*/; i++) std::cout << vertex_list[i] << " ";
	std::cout << "\n\n"; 

    //FILE * f = fopen("mt_bf.txt", "a");
    //fprintf(f, "%f\n", sec / 1000000);
    //fclose(f);

	pthread_attr_destroy(&attr);
	rc = pthread_mutex_destroy(&sec_lock); if (rc) assert(0);
	rc =  pthread_barrier_destroy(&barrier); if (rc) assert(0);

	for (int i = 0; i < num_vertices; i++) delete [] adj_list[i];
	delete [] adj_list;
	
    delete [] vertex_list;
	delete [] threads;
	delete [] build_threads;

	return 0;
}
