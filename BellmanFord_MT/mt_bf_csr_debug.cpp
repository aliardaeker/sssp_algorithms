/*
*
*	Ali Eker
*	Parallel implementation of Bellman Ford single source shortest path algorithm
*	07/29/2019
*
*	Directed Graph
*   ./mt_bf_csr <input file> <threads>
*/

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <bits/stdc++.h>
#include <pthread.h>
#include <math.h>

#define MAX 10000           // Maximum weight an edge can have

unsigned int * values;              // Are the value fields 0 initialized also ? I assume yes ..
unsigned int * column_indexes;
unsigned int * row_sizes_accum;
unsigned int * distances;
unsigned int * parents;

unsigned int N;             // Number of vertices
unsigned int NNZ;           // Number of non zero elements
unsigned int NCPUS;         // Number of processing threads

bool relaxed_global = false;
double sec = 0;

pthread_mutex_t sec_lock; 
pthread_barrier_t barrier;

struct barrier_time
{
	std ::chrono ::steady_clock ::time_point barrier_begin; 
	std ::chrono ::steady_clock ::time_point barrier_end; 
 	std ::chrono ::steady_clock ::time_point barrier_after;
};

struct time_measurements
{
    double between_barriers;
    double on_barriers;
};

double * run_times;
time_measurements * barrier_times;

inline 
void * 
run (void * id) 
{
    barrier_time * measurements = new barrier_time [N - 1];
	std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now();

	unsigned int tid = (intptr_t) id;
	unsigned int partition_size = N / NCPUS; 
	unsigned int range_begin = tid * partition_size;
	unsigned int range_end = tid * partition_size + partition_size - 1;
    unsigned int i, j, dist, new_dist, c_index; 
    int row_size_min, row_size_max, k; 
    bool relaxed;

    // CPU Affinity
    pthread_t thread = pthread_self();
    cpu_set_t cpuMask;

    CPU_ZERO(&cpuMask);
    CPU_SET(tid, &cpuMask);

    if (CPU_ISSET(tid, &cpuMask) >= 0)
    {
       int s = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask); 
       if (s != 0) perror("\nCPU Affinity Broken\n\n");
    }
    else perror("\nCPU Affinity Broken\n\n");

	for (i = 0; i < N - 1; i++)
	{
	    measurements[i].barrier_begin = std::chrono::steady_clock::now();
        relaxed = false;
        if (tid == 0) relaxed_global = false;
        
		for (j = range_begin; j <= range_end; j++)
		{
            row_size_min = row_sizes_accum[j];
            row_size_max = row_sizes_accum[j + 1]- 1;

            for (k = row_size_min; k <= row_size_max; k++)
            {
                c_index = column_indexes[k];
                dist = values[k];
                new_dist = dist + distances[j];

                if (new_dist < distances[c_index]) 
                {
                    relaxed = true;
                    distances[c_index]= new_dist;		
                    //parents[c_index].value = j;      
                }
            }
	    }	

	    pthread_mutex_lock(&sec_lock);	
        relaxed_global = relaxed_global | relaxed;
	    pthread_mutex_unlock(&sec_lock);	

        measurements[i].barrier_end = std::chrono::steady_clock::now();
		pthread_barrier_wait(&barrier);			
        if (!relaxed_global) break;
		pthread_barrier_wait(&barrier);			
        measurements[i].barrier_after = std::chrono::steady_clock::now();
	}

	std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
    run_times[tid] = sec_local / 1000000;
	
	pthread_mutex_lock(&sec_lock);	
	if (sec_local > sec) sec = sec_local;	
	pthread_mutex_unlock(&sec_lock);	

    double total_time = 0, total_barrier = 0;
    unsigned int iter = i + 1;

    for (unsigned int j = 0; j < i + 1; j++)
    {
        total_time += std::chrono::duration_cast<std::chrono::microseconds> (measurements[j].barrier_end - measurements[j].barrier_begin).count();
        if (j < iter - 1) total_barrier += std::chrono::duration_cast<std::chrono::microseconds> (measurements[j].barrier_after - measurements[j].barrier_end).count();
    }
    delete [] measurements;
    //printf("Total (%d): %d, %.5f, %.5f\n", tid, i, total_time, total_barrier);

    // Takes i and a half iterations actually. Because loop breaks i++ does not execute at the last iteration
    if (tid == 0) printf("After %d iterations ", i + 1); 
    
    barrier_times[tid].between_barriers = (total_time / (i + 1)) / 1000000;
    barrier_times[tid].on_barriers = (total_barrier / i) / 1000000;

    pthread_exit(NULL);
}

inline
void
read_file (std::ifstream & file)
{
    std::string line;
    std::getline(file, line);
    std::stringstream  nnz_stream(line);
    nnz_stream >> NNZ;

    std::getline(file, line);
    std::stringstream  n_stream(line);
    n_stream >> N;
    //assert(N % NCPUS == 0);

    values = new unsigned int [NNZ]; 
    column_indexes = new unsigned int [NNZ]; 
    row_sizes_accum = new unsigned int [N + 1];
    distances = new unsigned int [N];
    parents = new unsigned int [N];
    unsigned int counter = 0, val;

    std::getline(file, line);
    std::stringstream  values_stream(line);
    while (values_stream >> val) {values[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  column_stream(line);
    while (column_stream >> val) {column_indexes[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  rs_stream(line);
    while (rs_stream >> val) {row_sizes_accum[counter] = val; counter++;}
}

inline
void
check ()
{
    unsigned int i;
    std::cout << NNZ << "\n" << N << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", values[i]);
    std::cout << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", column_indexes[i]);
    std::cout << "\n";
    for (i = 0; i < N + 1; i++) printf("%d ", row_sizes_accum[i]);
    std::cout << "\n";
    for (i = 0; i < N; i++) printf("%d ", distances[i]);
    std::cout << "\n\n";
}

inline
void write_output()
{
    unsigned int i;
    FILE * f = fopen("data/distances_mt.txt", "w");
	for (i = 0; i < N; i++) fprintf(f, "%d\n", distances[i]); 

    f = fopen("data/paths_mt.txt", "w");
    for (i = 0; i < N; i++) fprintf(f, "%d\n", parents[i]); 

    f = fopen("data/runtime_mt.txt", "a");
    fprintf(f, "%f\n", sec / 1000000);
    fclose(f);
}

int 
main (int argc, char ** argv)
{
	assert(argc == 3);

	std::ifstream file(argv[1]); 
	NCPUS = atoi(argv[2]);
	pthread_t * threads = new pthread_t [NCPUS];
	run_times = new double [NCPUS];
	barrier_times = new time_measurements [NCPUS];

	std::cout << "\nReading the CSR file ...\n";
    std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now();
    read_file(file);
    std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	printf("CSR file is read. Took %.5f seconds.\n\n", sec_local / 1000000);

    unsigned int i;
    for (i = 0; i < N; i++) {parents[i] = MAX; distances[i] = MAX;}
    distances[0] = 0;  // Vertex 0 is the source
    //check(); 

    pthread_attr_t attr;
	void * status;
	int rc = pthread_mutex_init(&sec_lock, NULL); if (rc) assert(0);
	rc = pthread_barrier_init(&barrier, NULL, NCPUS); if (rc) assert(0);
	
    pthread_attr_init(&attr);
   	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);	
	std::cout << "Running multi-threaded Bellman-Ford ...\n";
	
	for (i = 0; i < NCPUS; i++) 
	{
      		rc = pthread_create(&threads[i], &attr, run, (void *) (intptr_t) i);
      		if (rc) assert(0);
    }
   	
	for (i = 0; i < NCPUS; i++) 
	{
     		rc = pthread_join(threads[i], &status);
      		if (rc) assert(0);
    }

	printf("Bellman-Ford converged. Took %.5f seconds.\n\n", sec / 1000000);
    write_output();

    //for (i = 0; i < NCPUS; i++) printf("%.5f ", run_times[i]);
    //std::cout << "\n\n";

    //for (i = 0; i < NCPUS; i++) printf("Tid %d: %.5f, %.5f\n", i, barrier_times[i].between_barriers, barrier_times[i].on_barriers);
    //std::cout << "\n\n";

    double avg_btw = 0, avg_on = 0;
    for (i = 0; i < NCPUS; i++) {avg_btw += barrier_times[i].between_barriers; avg_on += barrier_times[i].on_barriers;}
    avg_btw /= NCPUS;
    avg_on /= NCPUS;

    double std_btw = 0, std_on = 0;
    for (i = 0; i < NCPUS; i++) 
    {
        std_btw += std::pow(avg_btw - barrier_times[i].between_barriers, 2);
        std_on += std::pow(avg_on - barrier_times[i].on_barriers, 2);
    }  
    std_btw /= NCPUS; std_btw = sqrt(std_btw);
    std_on /= NCPUS; std_on = sqrt(std_on); 

    printf("Between Barriers -> avg: %.5f, btw std: %.5f\n", avg_btw, std_btw);
    printf("On Barriers -> avg: %.5f, btw std: %.5f\n\n", avg_on, std_on);


	pthread_attr_destroy(&attr);
	rc = pthread_mutex_destroy(&sec_lock); if (rc) assert(0);
	rc =  pthread_barrier_destroy(&barrier); if (rc) assert(0);

    delete [] values;
    delete [] column_indexes;
    delete [] row_sizes_accum;
    delete [] distances;
	delete [] threads;
    delete [] parents;
    delete [] run_times;
    delete [] barrier_times;

	return 0;
}
