/*
*
*	Ali Eker
*	Parallel implementation of Bellman Ford single source shortest path algorithm
*	07/29/2019
*
*	Directed Graph
*   ./mt_bf_csr <input file> <threads>
*/

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <bits/stdc++.h>
#include <pthread.h>
#include <math.h>

unsigned long long MAX = 4294967295; // Maximum weight an edge can have

unsigned int * values;              // Are the value fields 0 initialized also ? I assume yes ..
unsigned int * column_indexes;
unsigned int * row_sizes_accum;
unsigned int * distances;
//unsigned int * parents;

unsigned int N;             // Number of vertices
unsigned int NNZ;           // Number of non zero elements
unsigned int NCPUS;         // Number of processing threads

bool relaxed_global = false;
double sec = 0;

pthread_mutex_t sec_lock; 
pthread_barrier_t barrier;

inline 
void * 
run (void * id) 
{
	std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now();

	unsigned int tid = (intptr_t) id;
	unsigned int partition_size = N / NCPUS; 
	unsigned int range_begin = tid * partition_size;
	unsigned int range_end = tid * partition_size + partition_size - 1;
    
    if (tid == NCPUS - 1) range_end = N - 1;

    unsigned int i, j, dist, new_dist, c_index; 
    int row_size_min, row_size_max, k; 
    bool relaxed;

    // CPU Affinity
    pthread_t thread = pthread_self();
    cpu_set_t cpuMask;

    CPU_ZERO(&cpuMask);
    CPU_SET(tid, &cpuMask);

    if (CPU_ISSET(tid, &cpuMask) >= 0)
    {
       int s = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask); 
       if (s != 0) perror("\nCPU Affinity Broken\n\n");
    }
    else perror("\nCPU Affinity Broken\n\n");

	for (i = 0; i < N - 1; i++)
	{
        relaxed = false;
        if (tid == 0) relaxed_global = false;
        
		for (j = range_begin; j <= range_end; j++)
		{
            row_size_min = row_sizes_accum[j];
            row_size_max = row_sizes_accum[j + 1]- 1;

            for (k = row_size_min; k <= row_size_max; k++)
            {
                c_index = column_indexes[k];
                dist = values[k];
                new_dist = dist + distances[j];

                if (new_dist < distances[c_index]) 
                {
                    relaxed = true;
                    distances[c_index]= new_dist;		
                    //parents[c_index].value = j;      
                }
            }
	    }	

	    pthread_mutex_lock(&sec_lock);	
        relaxed_global = relaxed_global | relaxed;
	    pthread_mutex_unlock(&sec_lock);	

		pthread_barrier_wait(&barrier);			
        if (!relaxed_global) break;
		pthread_barrier_wait(&barrier);			
	}

	std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	
	pthread_mutex_lock(&sec_lock);	
	if (sec_local > sec) sec = sec_local;	
	pthread_mutex_unlock(&sec_lock);	

   // Takes i and a half iterations actually. Because loop breaks i++ does not execute at the last iteration
    if (tid == 0) printf("After %d iterations ", i + 1); 

    pthread_exit(NULL);
}

inline
void
read_file (std::ifstream & file)
{
    std::string line;
    std::getline(file, line);
    std::stringstream  nnz_stream(line);
    nnz_stream >> NNZ;

    std::getline(file, line);
    std::stringstream  n_stream(line);
    n_stream >> N;
    //assert(N % NCPUS == 0);

    values = new unsigned int [NNZ]; 
    column_indexes = new unsigned int [NNZ]; 
    row_sizes_accum = new unsigned int [N + 1];
    distances = new unsigned int [N];
    //parents = new unsigned int [N];
    unsigned int counter = 0, val;

    std::getline(file, line);
    std::stringstream  values_stream(line);
    while (values_stream >> val) {values[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  column_stream(line);
    while (column_stream >> val) {column_indexes[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  rs_stream(line);
    while (rs_stream >> val) {row_sizes_accum[counter] = val; counter++;}
}

inline
void
check ()
{
    unsigned int i;
    std::cout << NNZ << "\n" << N << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", values[i]);
    std::cout << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", column_indexes[i]);
    std::cout << "\n";
    for (i = 0; i < N + 1; i++) printf("%d ", row_sizes_accum[i]);
    std::cout << "\n";
    for (i = 0; i < N; i++) printf("%d ", distances[i]);
    std::cout << "\n\n";
}

inline
void write_output()
{
    FILE * f = fopen("data/distances_mt.txt", "w");
	for (unsigned i = 0; i < N; i++) fprintf(f, "%d\n", distances[i]); 
    fclose(f);

    //f = fopen("data/paths_mt.txt", "w");
    //for (i = 0; i < N; i++) fprintf(f, "%d\n", parents[i]); 

    FILE * f2 = fopen("data/runtime_mt.txt", "a");
    fprintf(f2, "%f\n", sec / 1000000);
    fclose(f2);
}

int 
main (int argc, char ** argv)
{
	assert(argc == 3);

	std::ifstream file(argv[1]); 
	NCPUS = atoi(argv[2]);
	pthread_t * threads = new pthread_t [NCPUS];

	std::cout << "\nReading the CSR file ...\n";
    std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now();
    read_file(file);
    std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	printf("CSR file is read. Took %.5f seconds.\n\n", sec_local / 1000000);

    unsigned int i;
    //for (i = 0; i < N; i++) {parents[i] = MAX; distances[i] = MAX;}
    for (i = 0; i < N; i++) distances[i] = MAX;
    distances[0] = 0;  // Vertex 0 is the source
    //check(); 

    pthread_attr_t attr;
	void * status;
	int rc = pthread_mutex_init(&sec_lock, NULL); if (rc) assert(0);
	rc = pthread_barrier_init(&barrier, NULL, NCPUS); if (rc) assert(0);
	
    pthread_attr_init(&attr);
   	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);	
	std::cout << "Running multi-threaded Bellman-Ford ...\n";
	
	for (i = 0; i < NCPUS; i++) 
	{
      		rc = pthread_create(&threads[i], &attr, run, (void *) (intptr_t) i);
      		if (rc) assert(0);
    }
   	
	for (i = 0; i < NCPUS; i++) 
	{
     		rc = pthread_join(threads[i], &status);
      		if (rc) assert(0);
    }

	printf("Bellman-Ford converged. Took %.5f seconds.\n\n", sec / 1000000);
    write_output();

	pthread_attr_destroy(&attr);
	rc = pthread_mutex_destroy(&sec_lock); if (rc) assert(0);
	rc =  pthread_barrier_destroy(&barrier); if (rc) assert(0);

    delete [] values;
    delete [] column_indexes;
    delete [] row_sizes_accum;
    delete [] distances;
	delete [] threads;
    //delete [] parents;

	return 0;
}
