/*
*
*	Ali Eker
*	Parallel implementation of Bellman Ford single source shortest path algorithm
*	07/29/2019
*
*   ./serial_bf_csr <input_file_name>
*
*/

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <bits/stdc++.h>

unsigned long long MAX = 4294967295;

unsigned int * values;              // Are the value fields 0 initialized also ? I assume yes ..
unsigned int * column_indexes;
unsigned int * row_sizes_accum;
unsigned int * distances;
//unsigned int * parents;

unsigned int N;             // Number of vertices
unsigned int NNZ;           // Number of non zero elements

inline 
void run () 
{
    unsigned int i, j, dist, new_dist, c_index;
    int row_size_min, row_size_max, k;
    bool relaxed;

	for (i = 0; i < N - 1; i++)
	{
        relaxed = false;
        
		for (j = 0; j < N; j++)
		{
            row_size_min = row_sizes_accum[j];
            row_size_max = row_sizes_accum[j + 1] - 1;

            for (k = row_size_min; k <= row_size_max; k++)
            {
                c_index = column_indexes[k];
                dist = values[k];
                new_dist = dist + distances[j];

                if (new_dist < distances[c_index]) 
                {
                    relaxed = true;
                    distances[c_index] = new_dist;		
                    //parents[c_index] = j;      
                }
            }
		}	

        if (!relaxed) break;
	}
}

inline
void
read_file (std::ifstream & file)
{
    std::string line;
    std::getline(file, line);
    std::stringstream  nnz_stream(line);
    nnz_stream >> NNZ;

    std::getline(file, line);
    std::stringstream  n_stream(line);
    n_stream >> N;

    values = new unsigned int [NNZ]; 
    column_indexes = new unsigned int [NNZ]; 
    row_sizes_accum = new unsigned int [N + 1];
    distances = new unsigned int [N];
    //parents = new unsigned int [N];
    unsigned int counter = 0, val;

    std::getline(file, line);
    std::stringstream  values_stream(line);
    while (values_stream >> val) {values[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  column_stream(line);
    while (column_stream >> val) {column_indexes[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  rs_stream(line);
    while (rs_stream >> val) {row_sizes_accum[counter] = val; counter++;}
}

inline
void write_output(double sec)
{
    FILE * f = fopen("data/distances_serial.txt", "w");
	for (unsigned i = 0; i < N; i++) fprintf(f, "%d\n", distances[i]); 
    fclose(f);

    //f = fopen("data/paths_serial.txt", "w");
	//for (i = 0; i < N; i++) fprintf(f, "%d\n", parents[i]); 

    FILE * f2 = fopen("data/runtime_serial.txt", "a");
    fprintf(f2, "%f\n", sec / 1000000);
    fclose(f2);
}

inline
void
check ()
{
    unsigned int i;
    std::cout << NNZ << "\n" << N << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", values[i]);
    std::cout << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", column_indexes[i]);
    std::cout << "\n";
    for (i = 0; i < N + 1; i++) printf("%d ", row_sizes_accum[i]);
    std::cout << "\n";
    for (i = 0; i < N; i++) printf("%d ", distances[i]);
    std::cout << "\n\n";
}

int main (int argc, char ** argv)
{
	assert(argc == 2);

    std::ifstream file(argv[1]); 

	std::cout << "\nReading the CSR file ...\n";
    std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now();
    read_file(file);
    std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	printf("CSR file is read. Took %.5f seconds.\n\n", sec_local / 1000000);

    
    //for (unsigned int i = 0; i < N; i++) {parents[i] = MAX; distances[i]= MAX;}
    for (unsigned int i = 0; i < N; i++) distances[i] = MAX;
    distances[0] = 0;  // Vertex 0 is the source
    //check(); 

    std::cout << "Running serial Bellman-Ford ...\n";
    begin = std::chrono::steady_clock::now();
    run();
    end = std::chrono::steady_clock::now();
	sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	printf("Bellman-Ford is done. Took %.5f seconds.\n\n", sec_local / 1000000);
    write_output(sec_local);

    delete [] values;
    delete [] column_indexes;
    delete [] row_sizes_accum;
    delete [] distances;
    //delete [] parents;

   	return 0;
}
