/*
*
*	Ali Eker
*	Parallel implementation of Bellman Ford single source shortest path algorithm
*	07/29/2019
*
*   ./serial_bf <num_vertices> <input_file_name>
*
*/

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <bits/stdc++.h>

int ** adj_list;
int * vertex_list;

int N;
#define MAX 10000

inline void run () 
{
    bool relaxed;

	for (int i = 0; i < N - 1; i++)
	{
        relaxed = false;
        
		for (int j = 0; j < N; j++)
		{
			for (int k = 0; k < N; k++)
			{
                //printf("checking %d %d -> %d: %d %d\n", j, k, adj_list[j][k], vertex_list[j], vertex_list[k]);
                int dist = adj_list[j][k];

                if (dist == 0) continue;
                int new_dist = vertex_list[j] + dist;

			    if (new_dist < vertex_list[k]) 
                {
                    relaxed = true;
                    vertex_list[k] = new_dist;		
                }
			}
		}	

        if (!relaxed) break;
	}
}

int main (int argc, char ** argv)
{
	if (argc != 3) assert(0);
    
    N = atoi(argv[1]); 

	vertex_list = new int [N];
	for (int i = 1; i < N; i++) vertex_list[i] = MAX;

	adj_list = new int * [N];	
	for (int i = 0; i < N; i++) adj_list[i] = new int[N];

    std::ifstream file(argv[2]);
    std::string line;
    int i = 0, j = 0;

    std::cout << "\nReading the Adj Matrix ...\n";
    while (std::getline(file, line))
    {
        std::stringstream stream(line);
        int val;

        while (stream >> val)
        {
            adj_list[i][j] = val;
            j++;
        }

        j = 0;
        i++;
    }
    std::cout << "Adj Matrix is read.\n\n";

    std::cout << "Running serial BF ...\n";
    run();
    std::cout << "Serial BF completed.\n\n";

    FILE * f = fopen("data/output_seq.txt", "w");
	for (int i = 0; i < N; i++) fprintf(f, "%d ", vertex_list[i]); 
    fclose(f);

	for (int i = 0; i < N; i++) delete [] adj_list[i];
	delete [] adj_list;
    delete [] vertex_list;

	return 0;
}
