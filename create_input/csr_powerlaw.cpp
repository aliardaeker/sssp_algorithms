/*
 *
 *  Ali Eker
 *  08/22/2019
 *  Constructs a power law graph in parallel.
 *  First creates an adjacency matrix and then convert it to CSR format and writes it to a file.
 *  ./csr <num_vertices> <skewness> <out file name csr> <out file name adj mat>
 *
 */

#include <iostream>
#include <assert.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <pthread.h>
#include <math.h>
#include <time.h>
#include <chrono>

#define T 64                 // Number of construction threads
#define WEIGHT 10           // Edge weights are between 1 and 10
typedef unsigned char edge; // Max edge weight is 255

unsigned int N;             // Number of vertices
unsigned int num_edges;     // Number of edges
float skewness;             // Power law skewness (alpha)
float sec = 0;

struct data                // For preventing false sharing
{
    unsigned int degree;
    int padding[15];
} __attribute__((packed));

edge ** adj_list;           // Global, static arrays are initialized to 0, locals not
data * degree_list;

pthread_mutex_t sec_lock; 
pthread_barrier_t barrier;

edge * final_values;
unsigned int * final_columns;
unsigned int * final_row_sizes;

volatile unsigned int merge_counter;
unsigned int edge_counter;
FILE * f;

inline 
void * 
construct (void * id)
{
    unsigned int tid = (intptr_t) id;
    
    if (tid == 0) std::cout << "\nConstructing the Adjacency Matrix...\n";
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now(); 

    unsigned int partition_size = N / T;
	unsigned int range_begin = tid * partition_size;
	unsigned int range_end = tid * partition_size + partition_size - 1;

    unsigned int seed = tid;
    unsigned int i, j, x, target;
    unsigned int local_num_edges = 0;
    int k, degree_size;
    double beta = pow(N, skewness);

    for (i = range_begin; i <= range_end; i++) adj_list[i] = new edge[N];

    for (i = range_begin; i <= range_end; i++)
    { 
        x = ((unsigned int) rand_r(&seed)) % N;
        
        if (x == 0) degree_list[i].degree = 0;
        else degree_list[i].degree = (unsigned int) floor(beta / pow(x, skewness));
    }
   
    for (i = range_begin; i <= range_end; i++)
	{
        degree_size = degree_list[i].degree;

		for (k = 0; k < degree_size; k++)
        {
	        target = ((unsigned int) rand_r(&seed)) % N;

            if (target == i)
            {
                k--;
                continue;
            }
            if (adj_list[i][target] == 0) 
            {
                adj_list[i][target] = ((unsigned int) rand_r(&seed) % WEIGHT) + 1;
                __sync_add_and_fetch(&num_edges, 1);
                local_num_edges++;
            }
        }
    }

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();	
	unsigned int sec_local = std::chrono::duration_cast <std::chrono::microseconds> (end - begin).count();
    
	pthread_mutex_lock(&sec_lock);	
	if (sec_local > sec) sec = sec_local;	
	pthread_mutex_unlock(&sec_lock);	
	pthread_barrier_wait(&barrier);			

    if (tid == 0)
    {
        printf("Adjacency Matrix is constructed. Took %.5f seconds.\n\n", sec / 1000000);
        std::cout << "Constructing the Compressed Sparse Row...\n";
        sec = 0;
    }
	begin = std::chrono::steady_clock::now(); 

    if (tid == 0)
    {
        // There should not be false shareing for these arrays because
        // threads access them in order
        final_values = new edge [num_edges];
        final_columns = new unsigned int [num_edges];
        final_row_sizes = new unsigned int [N + 1];
    }

    edge * values = new edge [local_num_edges];
    unsigned int * column_indexes = new unsigned int [local_num_edges];
    unsigned int * row_sizes_accum = new unsigned int [partition_size];

    unsigned int counter = 0, row_counter = 0;
    edge edge_value;

    for (i = range_begin; i <= range_end; i++, row_counter++)
	{
        for (j = 0; j < N; j++)
	    {
            edge_value = adj_list[i][j]; 

            if (edge_value)
            {
                values[counter] = edge_value; 
                column_indexes[counter] = j;
                counter++;
            }
        }

        row_sizes_accum[row_counter] = counter; 
    }
   
    assert(local_num_edges == counter);
    assert(partition_size == row_counter);
    unsigned int rs;

    // This is a Gather operation
    // Interesting Note: with -03 this block deadlocks, do not really know why. That is why merge_counter is volatile
    while (1)
    {
        if (tid == merge_counter)
        {  

            rs = final_row_sizes[tid * partition_size];
            for (i = 0; i < partition_size; i++) final_row_sizes[tid * partition_size + i + 1] = rs + row_sizes_accum[i];

            for (i = 0; i < local_num_edges; i++)
            {
               final_values[edge_counter] = values[i]; 
               final_columns[edge_counter] = column_indexes[i]; 
               edge_counter++;
            }
            
            merge_counter++;
            break;
        }
    }

	pthread_barrier_wait(&barrier);	
    assert(edge_counter == num_edges);
    assert(merge_counter == T);

    end = std::chrono::steady_clock::now();	
	sec_local = std::chrono::duration_cast <std::chrono::microseconds> (end - begin).count();
    
	pthread_mutex_lock(&sec_lock);	
	if (sec_local > sec) sec = sec_local;	
	pthread_mutex_unlock(&sec_lock);	
	pthread_barrier_wait(&barrier);	

    delete [] values;
    delete [] column_indexes;
    delete [] row_sizes_accum;

    if (tid == 0) 
    {
        printf("CSR is constructed. Took %.5f seconds.\n\n", sec / 1000000);
        std::cout << "Writing Compressed Sparse Row into a file...\n";
        sec = 0;
    }
	begin = std::chrono::steady_clock::now(); 

    if (tid == 0)
    {
        fprintf(f, "%d\n%d\n", num_edges, N);
        for (i = 0; i < num_edges; i++) fprintf(f, "%d ", final_values[i]);
        fprintf(f, "\n");
        for (i = 0; i < num_edges; i++) fprintf(f, "%d ", final_columns[i]);
        fprintf(f, "\n");
        for (i = 0; i < N + 1; i++) fprintf(f, "%d ", final_row_sizes[i]);
    }
   
    end = std::chrono::steady_clock::now();	
	sec_local = std::chrono::duration_cast <std::chrono::microseconds> (end - begin).count();
    
	pthread_mutex_lock(&sec_lock);	
	if (sec_local > sec) sec = sec_local;	
	pthread_mutex_unlock(&sec_lock);	
	pthread_barrier_wait(&barrier);	

    if (tid == 0) printf("CSR is written. Took %.5f seconds.\n\n", sec / 1000000);
    pthread_exit(NULL);
}

inline 
void * 
destroy (void * id)
{
    int tid = (intptr_t) id;
    int partition_size = N / T;
	int range_begin = tid * partition_size;
	int range_end = tid * partition_size + partition_size - 1;
    
    for (int i = range_begin; i <= range_end; i++) delete [] adj_list[i];

    pthread_exit(NULL);
}

int 
main (int argc, char ** argv)
{
    if (argc != 5) assert(0);

    N = atoi(argv[1]);                  // Should be power of T for even partitions. For T = 64: 4096, 32768, 65536, 131072 ..
    skewness = atof(argv[2]);           // Usually 2.1
    f = fopen(argv[3], "w");
    
    assert((N % T) == 0);
    //srand(time(NULL));

    void * status;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	int rc = pthread_mutex_init(&sec_lock, NULL); if (rc) assert(0);
	rc = pthread_barrier_init(&barrier, NULL, T); if (rc) assert(0);
    
    pthread_t * threads = new pthread_t [T]; 
    degree_list = new data [N];
    adj_list = new edge * [N];
    unsigned int i;

    for (i = 0; i < T; i++) 
	{
      		rc = pthread_create(&threads[i], &attr, construct, (void *) (intptr_t) i);
      		if (rc) assert(0);
    }
   	
	for (i = 0; i < T; i++) 
	{
     		rc = pthread_join(threads[i], &status);
      		if (rc) assert(0);
    }

    /*
    std::cout << "Degree Histogram: \n";
    int * arr = new int [N];
    memset(arr, 0, N);	
	for (i = 0; i < N; i++) for (unsigned int j = 0; j < N; j++) if (adj_list[i][j]) arr[i]++; 

    int * degree = new int [N];
    memset(degree, 0, N);	
	for (i = 0; i < N; i++) degree[arr[i]]++;
	for (i = 0; i < N; i++) printf("%d ", degree[i]); 
	std::cout << "\n\n"; 

    delete [] arr;
	delete [] degree;
    */

    //std::cout << num_edges << "\n";

    unsigned int j;
    std::cout << "Writing Adj Matrix into a file...\n";
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now(); 

    FILE * fw = fopen(argv[4], "w");
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++) fprintf(fw, "%d ", adj_list[i][j]);
        if (i < N - 1) fprintf(fw, "\n");
    }
    fclose(fw);
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast <std::chrono::microseconds> (end - begin).count();
    printf("Adj Matrix is written. Took %.5f seconds.\n\n", sec_local / 1000000);

    for (i = 0; i < T; i++) 
	{
      		rc = pthread_create(&threads[i], &attr, destroy, (void *) (intptr_t) i);
      		if (rc) assert(0);
    }
   	
	for (i = 0; i < T; i++) 
	{
     		rc = pthread_join(threads[i], &status);
      		if (rc) assert(0);
    }

    fclose(f);
    delete [] adj_list;
    delete [] degree_list;
    delete [] threads;

	pthread_attr_destroy(&attr);
	rc = pthread_mutex_destroy(&sec_lock); if (rc) assert(0);
	rc =  pthread_barrier_destroy(&barrier); if (rc) assert(0);

    delete [] final_values;
    delete [] final_columns;
    delete [] final_row_sizes;

    return 0;
}
