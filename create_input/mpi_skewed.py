import random

num_vertices = 1024
skewness = 2.1
prob = 1 / skewness

out_file = open('mpi_skewed_1K.txt','w')
out_file.write('%d\n' % num_vertices)

for i in range(num_vertices): # 3 -> 0, 1, 2
    for j in range(num_vertices):
        if i == j:
            out_file.write('0 ') # no self edge
        else:
            rand = random.uniform(0, 1) # between 0 and 1 

            if rand <= prob: 
                out_file.write('1 ') # all edge weights are 1
            else:
                out_file.write('1000000 ') # no edge 
        
    out_file.write('\n')

out_file.close()
