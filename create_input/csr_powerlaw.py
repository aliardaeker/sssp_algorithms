import random
import sys

#
# python csr_powerlaw.py <num vertices> <skewness> <out file name>  
#

num_vertices = int(sys.argv[1])
skewness = float(sys.argv[2])
prob = 1 / skewness

out_file = open(sys.argv[3],'w')

