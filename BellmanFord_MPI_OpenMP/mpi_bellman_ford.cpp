/*
 * This is a mpi version of bellman_ford algorithm
 * Compile: mpic++ -std=c++11 -o mpi_bellman_ford mpi_bellman_ford.cpp
 * Run: mpiexec -n <number of processes> ./mpi_bellman_ford <num_vertices> <skewness>
 * mpiexec -n 1 ./mpi_bf 256 2.1
 * */

#include <string>
#include <cassert>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <cstring>

#include <time.h>
#include <chrono>
#include <bits/stdc++.h>
#include <pthread.h>
#include <math.h>

#include "mpi.h"
#define THREADS 1 // Number of threads for building the graph
#define MAX 100000

using std::string;
using std::cout;
using std::endl;

int * degree_list;
pthread_mutex_t sec_lock; 
double sec = 0;
double probability;

/**
 * utils is a namespace for utility functions
 * including I/O (read input file and print results) and matrix dimension convert(2D->1D) function
 */
namespace utils 
{
    int N; //number of vertices
    int ** mat; // the adjacency matrix

    inline void * construct (void * id)
    {
	    std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now(); 

        int tid = (intptr_t) id;
        int partition_size = N / THREADS;
	    int range_begin = tid * partition_size;
	    int range_end = tid * partition_size + partition_size - 1;
        unsigned int seed = tid;
 
        double beta = pow(N, probability);
        int x, target;
     
        for (int i = range_begin; i <= range_end; i++) mat[i] = new int[N];

        for (int i = range_begin; i <= range_end; i++)
        {
            for (int j = 0; j < N; j++) mat[i][j] = 0;
        }

        for (int i = range_begin; i <= range_end; i++)
        { 
	        x = ((int) rand_r(&seed)) % utils::N;

            if (x == 0) degree_list[i] = 0;
            else degree_list[i] = (int) floor(beta / pow(x, probability)); 
        }
    
        for (int i = range_begin; i <= range_end; i++)
	    {
		    for (int k = 0; k < degree_list[i]; k++)
            {
	            target = ((int) rand_r(&seed)) % N;

                if (target == i)
                {
                    k--;
                    continue;
                }

                mat[i][target] = 1;

                //if (adj_list[i][target] == 0) adj_list[i][target] = 1;
                //else
                //{
                //    k--;
                //    continue;
                //}
            }
        }

        std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	    double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	
	    pthread_mutex_lock(&sec_lock);	
	    if (sec_local > sec) sec = sec_local;	
    	pthread_mutex_unlock(&sec_lock);	

        pthread_exit(NULL);
    }

    int generate_graph(int n) 
    {
        pthread_t * threads = new pthread_t [THREADS];
       	pthread_attr_t attr;
	    void * status;
	    int rc = pthread_mutex_init(&sec_lock, NULL); if (rc) assert(0);
 
        N = n;
        //mat = (int *) malloc(N * N * sizeof(int));
        mat = new int * [N];

        degree_list = new int [N];
        cout << "\nConstructing the graph ...\n"; 
    
        pthread_attr_init(&attr);
   	    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);	
	
	    for (int i = 0; i < THREADS; i++) 
	    {
      		rc = pthread_create(&threads[i], &attr, construct, (void *) (intptr_t) i);
      		if (rc) assert(0);
        }
   	
	    for (int i = 0; i < THREADS; i++) 
	    {
     		rc = pthread_join(threads[i], &status);
      		if (rc) assert(0);
        }

	    printf("Constructing the graph is done. Took %.5f seconds.\n\n", sec / 1000000);
        
        //for (int i = 0; i < N; i++) 
        //{
        //    for (int j = 0; j < N; j++) cout << mat[convert_dimension_2D_1D(i, j, N)] << " "; 
        //    cout << "\n";
        //}
        
        /*
        int * arr = new int [N];
	    for (int i = 0; i < N; i++) arr[i] = 0;

	    std::cout << "Degree Histogram: \n";
	    for (int i = 0; i < N; i++) 
        {
            for (int j = 0; j < N; j++) if (mat[i][j] == 1) arr[i]++; 
        }

        int * degree = new int [N];
	    for (int i = 0; i < N; i++) degree[i] = 0;

	    for (int i = 0; i < N; i++) degree[arr[i]]++;
	    for (int i = 0; i < N; i++) printf("%d ", degree[i]); 
	    std::cout << "\n\n"; 

        delete [] arr;
	    delete [] degree; 
        */

        delete [] degree_list;
        return 0;
    }
}


/**
 * Bellman-Ford algorithm. Find the shortest path from vertex 0 to other vertices.
 * @param my_rank the rank of current process
 * @param p number of processes
 * @param comm the MPI communicator
 * @param n input size
 * @param *mat input adjacency matrix
 * @param *dist distance array
 * @param *has_negative_cycle a bool variable to recode if there are negative cycles
*/
void bellman_ford(int my_rank, int p, MPI_Comm comm, int n, int * mat, int * dist) 
{
    int loc_n; // need a local copy for N
    int loc_start, loc_end;
    int * loc_mat; //local matrix
    int * loc_dist; //local distance

    //step 1: broadcast N
    if (my_rank == 0) loc_n = n;

    MPI_Bcast(&loc_n, 1, MPI_INT, 0, comm);

    //step 2: find local task range
    int ave = loc_n / p;
    loc_start = ave * my_rank;
    loc_end = ave * (my_rank + 1);
    if (my_rank == p - 1) loc_end = loc_n;

    //step 3: allocate local memory
    loc_mat = (int *) malloc(loc_n * loc_n * sizeof(int));
    loc_dist = (int *) malloc(loc_n * sizeof(int));

    //step 4: broadcast matrix mat
    if (my_rank == 0) memcpy(loc_mat, mat, sizeof(int) * loc_n * loc_n);
    MPI_Bcast(loc_mat, loc_n * loc_n, MPI_INT, 0, comm);

    //step 5: bellman-ford algorithm
    for (int i = 0; i < loc_n; i++) loc_dist[i] = MAX;
    loc_dist[0] = 0;

    MPI_Barrier(comm);

    bool loc_has_change;
    int new_dist;

    for (int iter = 0; iter < loc_n - 1; iter++) // Traverse loc_n = whole input size
    {
        loc_has_change = false;

        for (int u = loc_start; u < loc_end; u++) // Traverse local range
        {
            for (int v = 0; v < loc_n; v++) // Traverse loc_n
            {
                if (loc_mat[u][v] > 0)
                {
                    new_dist = loc_dist[u] + loc_mat[u][v];
                
                    if (new_dist < loc_dist[v]) 
                    {
                        loc_has_change = true;
                        loc_dist[v] = new_dist;
                    }
                }
            }
        }

        MPI_Allreduce(MPI_IN_PLACE, &loc_has_change, 1, MPI_CXX_BOOL, MPI_LOR, comm);
        if (!loc_has_change) break;

        MPI_Allreduce(MPI_IN_PLACE, loc_dist, loc_n, MPI_INT, MPI_MIN, comm);
    }

    //step 6: retrieve results back
    if(my_rank == 0) memcpy(dist, loc_dist, loc_n * sizeof(int));

    //step 7: remember to free memory
    free(loc_mat);
    free(loc_dist);
}

int main(int argc, char **argv) 
{
    if (argc != 3) assert(false);
    
    int num_vertices = atoi(argv[1]); // Should be power of 32 -> 1024, 8192, 131072 (128K), 262144 (256K)
    probability = atof(argv[2]);	

    int * dist = 0;

    //MPI initialization
    MPI_Init(&argc, &argv);
    MPI_Comm comm;

    int p;//number of processors
    int my_rank;//my global rank
    comm = MPI_COMM_WORLD;
    MPI_Comm_size(comm, &p);
    MPI_Comm_rank(comm, &my_rank);

    //only rank 0 process do the I/O
    if (my_rank == 0) 
    {
        assert(utils::generate_graph(num_vertices) == 0);
        dist = (int *) malloc(sizeof(int) * utils::N);
    }

    //time counter
    double t1, t2;
    MPI_Barrier(comm);

    if (my_rank == 0) cout << "Running the Bellman-Ford for sssp ...\n";
    t1 = MPI_Wtime();

    //bellman-ford algorithm
    bellman_ford(my_rank, p, comm, utils::N, utils::mat, dist);
    MPI_Barrier(comm);

    //end timer
    t2 = MPI_Wtime();

    if (my_rank == 0) 
    {
	    printf("Sssp is done. Took %.5f seconds.\n\n", t2 - t1);

        cout << "Vertex List: \n";
        for (int i = 0; i < 32 /*num_vertices*/; i++) cout << dist[i] << " ";
        cout << "\n\n"; 

        free(dist);
        free(utils::mat);
    
        //FILE * f = fopen("bf.txt", "a");
        //fprintf(f, "%f\n", t2 - t1);
        //fclose(f);
    }

    MPI_Finalize();
    return 0;
}
