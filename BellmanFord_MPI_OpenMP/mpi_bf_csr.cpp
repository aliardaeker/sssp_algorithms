/*
 * This is a mpi version of bellman_ford algorithm
 * Compile: mpic++ -std=c++11 -o mpi_bellman_ford mpi_bellman_ford.cpp
 * Run: mpiexec -n <number of processes> ./mpi_bellman_ford <num_vertices> <skewness>
 * mpiexec -n <num processes> ./mpi_bf_csr <input file>
 * */

#include <string>
#include <cassert>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <cstring>
#include <time.h>
#include <chrono>
#include <bits/stdc++.h>
#include <pthread.h>
#include <math.h>
#include "mpi.h"

#define MAX 10000
typedef unsigned int data;

data * values;              // Are the value fields 0 initialized also ? I assume yes ..
data * column_indexes;
data * row_sizes_accum;
data * distances;           // With mpic++ compiler these are not initialized to 0 ...
//unsigned int * parents;

unsigned int N;             // Number of vertices
unsigned int NNZ;           // Number of non zero elements

inline
void 
bellman_ford (int my_rank, int p, MPI_Comm comm) 
{
    unsigned int loc_start, loc_end;
    unsigned int ave = N / p;

    loc_start = ave * my_rank;
    loc_end = ave * (my_rank + 1);
    if (my_rank == p - 1) loc_end = N;

    bool relaxed;
    unsigned int dist, new_dist, i, j, c_index;
    int row_size_min, row_size_max, k;

    for (i = 0; i < N - 1; i++)      // Traverse N = whole input size
    {
        relaxed = false;

        for (j = loc_start; j < loc_end; j++) // Traverse local range
        {
            row_size_min = row_sizes_accum[j];
            row_size_max = row_sizes_accum[j + 1] - 1;

            for (k = row_size_min; k <= row_size_max; k++)
            {
                c_index = column_indexes[k];
                dist = values[k];
                new_dist = dist + distances[j];

                if (new_dist < distances[c_index]) 
                {
                    
                    relaxed = true;
                    distances[c_index] = new_dist;		
                    //parents[c_index] = j;
                }
            }
        }

        MPI_Allreduce(MPI_IN_PLACE, &relaxed, 1, MPI_CXX_BOOL, MPI_LOR, comm);

        if (!relaxed) break;

        MPI_Allreduce(MPI_IN_PLACE, distances, N, MPI_UNSIGNED, MPI_MIN, comm);
        //MPI_Allreduce(MPI_IN_PLACE, parents, N, MPI_UNSIGNED, MPI_MIN, comm); // This is wrong
    }
}

inline
void
read_file (std::ifstream & file)
{
    std::string line;
    std::getline(file, line);
    std::stringstream  nnz_stream(line);
    nnz_stream >> NNZ;

    std::getline(file, line);
    std::stringstream  n_stream(line);
    n_stream >> N;

    values = new data [NNZ]; 
    column_indexes = new data [NNZ]; 
    row_sizes_accum = new data [N + 1];
    distances = new data [N];
   
    //parents = new unsigned int [N];
    unsigned int counter = 0, val;

    std::getline(file, line);
    std::stringstream  values_stream(line);
    while (values_stream >> val) {values[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  column_stream(line);
    while (column_stream >> val) {column_indexes[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  rs_stream(line);
    while (rs_stream >> val) {row_sizes_accum[counter] = val; counter++;}
}

inline
void
check ()
{
    unsigned int i;
    std::cout << NNZ << "\n" << N << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", values[i]);
    std::cout << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", column_indexes[i]);
    std::cout << "\n";
    for (i = 0; i < N + 1; i++) printf("%d ", row_sizes_accum[i]);
    std::cout << "\n";
    for (i = 0; i < N; i++) printf("%d ", distances[i]);
    std::cout << "\n\n";
}

inline
void write_output(double sec)
{
    //unsigned int i;
    FILE * f;// = fopen("data/distances_mpi.txt", "w");
	//for (i = 0; i < N; i++) fprintf(f, "%d\n", distances[i]); 

    //f = fopen("data/paths_mpi.txt", "w");
    //for (i = 0; i < N; i++) fprintf(f, "%d\n", parents[i]);

    f = fopen("data/runtime_mpi.txt", "a");
    fprintf(f, "%f\n", sec / 1000000);
    fclose(f);
}

int 
main (int argc, char **argv) 
{
    assert(argc == 2);

	MPI_Init(&argc, &argv); //MPI initialization
    MPI_Comm comm = MPI_COMM_WORLD;

    int p;                  //number of processors
    int my_rank;            //my global rank
    MPI_Comm_size(comm, &p);
    MPI_Comm_rank(comm, &my_rank);
    //assert(N % p == 0);

    std::ifstream file(argv[1]); 
    std ::chrono ::steady_clock ::time_point begin, end;
    double sec_local;

    MPI_Barrier(comm);
    if (my_rank == 0)
    {
	    std::cout << "\nReading the CSR file ...\n";
        begin = std::chrono::steady_clock::now();
    }

    read_file(file);
    MPI_Barrier(comm);

    if (my_rank == 0)
    {
        end = std::chrono::steady_clock::now();	
	    sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	    printf("CSR file is read. Took %.5f seconds.\n\n", sec_local / 1000000);
    }

    for (unsigned int i = 0; i < N; i++) {distances[i]= MAX;} //parents[i] = MAX;  
    distances[0] = 0;  // Vertex 0 is the source
    //if (my_rank == 0) check();

    MPI_Barrier(comm);
    if (my_rank == 0)
    {
	    std::cout << "Running MPI Bellman-Ford ...\n";
        begin = std::chrono::steady_clock::now();
    }
    
    bellman_ford(my_rank, p, comm);
	
    if (my_rank == 0)
    {
        end = std::chrono::steady_clock::now();	
	    sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
        printf("Bellman-Ford is done. Took %.5f seconds.\n\n", sec_local / 1000000);
        write_output(sec_local);
    }

    delete [] values;
    delete [] column_indexes;
    delete [] row_sizes_accum;
    delete [] distances;
    //delete [] parents;

    MPI_Finalize();
    return 0;
}
