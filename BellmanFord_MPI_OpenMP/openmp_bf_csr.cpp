/*
 * This is a openmp version of bellman_ford algorithm
 * Compile: g++ -std=c++11 -o openmp_bellman_ford openmp_bellman_ford.cpp
 * Run: ./openmp_bf_csr <input file> <number of threads>
 * */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cassert>
#include <algorithm>
#include <cstring>
#include <time.h>
#include <chrono>

#include "omp.h"
#define MAX 10000

unsigned int * values;              // Are the value fields 0 initialized also ? I assume yes ..
unsigned int * column_indexes;
unsigned int * row_sizes_accum;
unsigned int * distances;

unsigned int N;             // Number of vertices
unsigned int NNZ;           // Number of non zero elements
unsigned int NCPUS;         // Number of processing threads

inline
void 
bellman_ford () 
{
    unsigned int partition_size = N / NCPUS;
    bool relaxed_global;           

    omp_set_dynamic(0); // Without this NCPUS is just an upper limit on the number of threads 
    omp_set_num_threads(NCPUS);

    #pragma omp parallel    
    {
        unsigned int tid = omp_get_thread_num();
        unsigned int range_begin = tid * partition_size;
        unsigned int range_end = range_begin + partition_size - 1;

        if (tid == NCPUS - 1) range_end = N - 1;

        unsigned i, j, dist, new_dist, c_index;
        int row_size_min, row_size_max, k;
        bool relaxed;

        for (i = 0; i < N - 1; i++) 
        {
            relaxed = false;
            if (tid == 0) relaxed_global = false;

            for (j = range_begin; j <= range_end; j++) 
            {
                row_size_min = row_sizes_accum[j];
                row_size_max = row_sizes_accum[j + 1] - 1;

                for (k = row_size_min; k <= row_size_max; k++)
                {
                    c_index = column_indexes[k];
                    dist = values[k];
                    new_dist = dist + distances[j];

                    if (new_dist < distances[c_index]) 
                    {
                        relaxed = true;
                        distances[c_index] = new_dist;		
                    }
                }
            }

            #pragma omp critical 
            relaxed_global = relaxed_global | relaxed;
            #pragma omp barrier

            if (!relaxed_global)  break;
            #pragma omp barrier
        }
    }
}

inline
void
read_file (std::ifstream & file)
{
    std::string line;
    std::getline(file, line);
    std::stringstream  nnz_stream(line);
    nnz_stream >> NNZ;

    std::getline(file, line);
    std::stringstream  n_stream(line);
    n_stream >> N;
    //assert(N % NCPUS == 0);

    values = new unsigned int [NNZ]; 
    column_indexes = new unsigned int [NNZ]; 
    row_sizes_accum = new unsigned int [N + 1];
    distances = new unsigned int [N];
    unsigned int counter = 0, val;

    std::getline(file, line);
    std::stringstream  values_stream(line);
    while (values_stream >> val) {values[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  column_stream(line);
    while (column_stream >> val) {column_indexes[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  rs_stream(line);
    while (rs_stream >> val) {row_sizes_accum[counter] = val; counter++;}
}

inline 
void
check ()
{
    unsigned int i;
    std::cout << NNZ << "\n" << N << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", values[i]);
    std::cout << "\n";
    for (i = 0; i < NNZ; i++) printf("%d ", column_indexes[i]);
    std::cout << "\n";
    for (i = 0; i < N + 1; i++) printf("%d ", row_sizes_accum[i]);
    std::cout << "\n";
    for (i = 0; i < N; i++) printf("%d ", distances[i]);
    std::cout << "\n\n";
}

inline
void write_output(double sec)
{
    //unsigned int i;
    FILE * f;// = fopen("data/distances_openmp.txt", "w");
	//for (i = 0; i < N; i++) fprintf(f, "%d\n", distances[i]); 

    f = fopen("data/runtime_openmp.txt", "a");
    fprintf(f, "%f\n", sec / 1000000);
    fclose(f);
}

int 
main(int argc, char ** argv) 
{
    assert(argc == 3);
    std::ifstream file(argv[1]); 
	NCPUS = atoi(argv[2]);

	std::cout << "\nReading the CSR file ...\n";
    std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now();
    read_file(file);
    std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	printf("CSR file is read. Took %.5f seconds.\n\n", sec_local / 1000000);

    for (unsigned i = 1; i < N; i++) distances[i] = MAX; // Vertex 0 is the source
    //check();

	std::cout << "Running openmp Bellman-Ford ...\n";
    begin = std::chrono::steady_clock::now();
    bellman_ford();
    end = std::chrono::steady_clock::now();
    sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	printf("Bellman-Ford is done. Took %.5f seconds.\n\n", sec_local / 1000000);
    write_output(sec_local);
    
    delete [] values;
    delete [] column_indexes;
    delete [] row_sizes_accum;
    delete [] distances;

    return 0;
}
