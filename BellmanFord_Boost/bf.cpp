/*
*
*	Ali Eker
*	Parallel implementation of Bellman Ford single source shortest path algorithm
*	Using Boost Library
*	08/19/2019
*
*	Directed Graph
* 	All edge weights are 1
*/

#include <iostream>
#include <cstdlib>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <bits/stdc++.h>

#include <boost/config.hpp>

//#include <pthread.h>

int ** adj_list;
int * vertex_list;

int N;
int NCPUS;
double sec = 0;

//pthread_mutex_t sec_lock; 
//pthread_barrier_t barrier;
//
inline void run () 
{
	//int tid = (intptr_t) id;
	//int partition_size = N / NCPUS; 
	//int range_begin = tid * partition_size;
	//int range_end = tid * partition_size + partition_size - 1;

    // CPU Affinity
    /*
    pthread_t thread = pthread_self();
    cpu_set_t cpuMask;

    CPU_ZERO(&cpuMask);
    CPU_SET(tid, &cpuMask);

    if (CPU_ISSET(tid, &cpuMask) >= 0)
    {
       int s = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask); 
       if (s != 0) perror("\nCPU Affinity Broken\n\n");
    }
    else perror("\nCPU Affinity Broken\n\n");
    */

	std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now();

	std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
	double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
	
	//pthread_mutex_lock(&sec_lock);	
	if (sec_local > sec) sec = sec_local;	
	//pthread_mutex_unlock(&sec_lock);	

	//pthread_exit(NULL);
}

int main (int argc, char ** argv)
{
	if (argc != 4) assert(0);

	int num_vertices = atoi(argv[1]); // Should be power of 32 -> 1024, 8192, 16384, 131072
	N = num_vertices;

	double skewness = atof(argv[2]);
	if (skewness < 1) assert(0); // Should be >= 1	
	double probability = 1 / skewness; // Higher the alpha, lesser the density and more low degree vetrtices
	// srand(time(NULL));

	int ncpus = atoi(argv[3]);
	//pthread_t * threads = new pthread_t [ncpus];
	NCPUS = ncpus;
	
	//pthread_attr_t attr;
	//void * status;
	//int rc = pthread_mutex_init(&sec_lock, NULL); if (rc) assert(0);
	//rc = pthread_barrier_init(&barrier, NULL, ncpus); if (rc) assert(0);
	
	std::cout << "\nConstructing the graph ...\n";
	
	vertex_list = new int [num_vertices];
	vertex_list[0] = 0; // Vertex 0 is the source
	for (int i = 1; i < num_vertices; i++) vertex_list[i] = INT_MAX - 1;

	adj_list = new int * [num_vertices];	
	for (int i = 0; i < num_vertices; i++) adj_list[i] = new int[num_vertices];

	for (int i = 0; i < num_vertices; i++)
	{
		for (int j = 0; j < num_vertices; j++)
		{
			if (i == j) adj_list[i][j] = 0; // no self edge
			else
			{
				double r = ((double) rand() / (RAND_MAX));
				
                if (r < probability) adj_list[i][j] = 1; // All edges weight to 1
				else adj_list[i][j] = 0; // no edge
			}
		}
	}
	
	std::cout << "Constructing the graph is done.\n\nRunning the Bellman-Ford for sssp ...\n";
	
	//pthread_attr_init(&attr);
   	//pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);	
	
    
    run();

    /*
	for (int i = 0; i < ncpus; i++) 
	{
      		rc = pthread_create(&threads[i], &attr, run, (void *) (intptr_t) i);
      		if (rc) assert(0);
    }
   	
	for (int i = 0; i < ncpus; i++) 
	{
     		rc = pthread_join(threads[i], &status);
      		if (rc) assert(0);
    }
    */

	printf("Sssp is done. Took %.3f seconds.\n\n", sec / 1000000);

	//std::cout << "Vertex List: \n";
	//for (int i = 0; i < num_vertices; i++) std::cout << vertex_list[i] << " ";
	//std::cout << "\n\n"; 


    //FILE * f = fopen("bf_boost.txt", "a");
    //fprintf(f, "%f\n", sec / 1000000);
    //fclose(f);

	//pthread_attr_destroy(&attr);
	//rc = pthread_mutex_destroy(&sec_lock); if (rc) assert(0);
	//rc =  pthread_barrier_destroy(&barrier); if (rc) assert(0);

	for (int i = 0; i < num_vertices; i++) delete [] adj_list[i];
	delete [] adj_list;
	
    delete [] vertex_list;
	//delete [] threads;

	return 0;
}
