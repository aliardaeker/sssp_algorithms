/*
*
*   ./dijkstra <input_file_name>
*
*/

#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <limits> // for numeric_limits
#include <set>
#include <utility> // for pair
#include <algorithm>
#include <iterator>
#include <time.h>
#include <chrono>
#include <bits/stdc++.h>

unsigned long long MAX = 4294967295; 

unsigned int * values;              // Are the value fields 0 initialized also ? I assume yes ..
unsigned int * column_indexes;
unsigned int * row_sizes_accum;

unsigned int N;             // Number of vertices
unsigned int NNZ;           // Number of non zero elements

typedef unsigned int vertex_t;
typedef unsigned int weight_t;

std::pair <std::set <std::pair <weight_t, vertex_t>> ::iterator, bool> * iters;

inline 
void 
DijkstraComputePaths (vertex_t source, std::vector <weight_t> & min_distance)
{
    min_distance.clear();
    min_distance.resize(N, MAX);
    min_distance[source] = 0;

    std::set <std::pair <weight_t, vertex_t>> vertex_queue;
    vertex_queue.insert(std::make_pair(min_distance[source], source));

    int row_size_min, row_size_max, k;
    vertex_t v;
    weight_t weight;

    while (!vertex_queue.empty()) 
    {
        weight_t dist = vertex_queue.begin() -> first;
        vertex_t u = vertex_queue.begin() -> second;
        vertex_queue.erase(vertex_queue.begin());
 
        row_size_min = row_sizes_accum[u];
        row_size_max = row_sizes_accum[u + 1] - 1;

        // Visit each edge exiting u
        for (k = row_size_min; k <= row_size_max; k++)
        {
            v = column_indexes[k];
            weight = values[k];
            weight_t distance_through_u = dist + weight;

            assert(v != 2580773);

            if (distance_through_u < min_distance[v]) // relax: 1 < v -> dist
            {
	            if (iters[v].second)
		        {
			        vertex_queue.erase(iters[v].first);
		        	iters[v].second = false; 
		        }

	            min_distance[v] = distance_through_u;
		        iters[v] = vertex_queue.insert(std::make_pair(min_distance[v], v));
	        }
        }
    }
}

inline
void
read_file (std::ifstream & file)
{
    std::string line;
    std::getline(file, line);
    std::stringstream  nnz_stream(line);
    nnz_stream >> NNZ;

    std::getline(file, line);
    std::stringstream  n_stream(line);
    n_stream >> N;

    iters = new std::pair <std::set <std::pair <weight_t, vertex_t>> ::iterator, bool> [N];
    for (unsigned i = 0; i < N; i++) iters[i].second = false;

    values = new unsigned int [NNZ]; 
    column_indexes = new unsigned int [NNZ]; 
    row_sizes_accum = new unsigned int [N + 1];
    unsigned int counter = 0, val;

    std::getline(file, line);
    std::stringstream  values_stream(line);
    while (values_stream >> val) {values[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  column_stream(line);
    while (column_stream >> val) {column_indexes[counter] = val; counter++;}
    counter = 0;

    std::getline(file, line);
    std::stringstream  rs_stream(line);
    while (rs_stream >> val) {row_sizes_accum[counter] = val; counter++;}
}

inline
void 
write_output(std::vector <weight_t> & d, double sec)
{
    assert(d.size() == N);
    FILE * f = fopen("data/distances_dijkstra.txt", "w");
    for (unsigned i = 0; i < N; i++) fprintf(f, "%d\n", d[i]); 
    fclose(f);

    FILE * f_2 = fopen("data/runtime_dijkstra.txt", "a");
    fprintf(f_2, "%f\n", sec / 1000000);
    fclose(f_2);
}

int main (int argc, char ** argv)
{
    assert(argc == 2);
    std::ifstream file(argv[1]); 

    std::cout << "\nReading the CSR file ...\n";
    std ::chrono ::steady_clock ::time_point begin = std::chrono::steady_clock::now();
    read_file(file);
    std ::chrono ::steady_clock ::time_point end = std::chrono::steady_clock::now();	
    double sec_local = std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count();
    printf("CSR file is read. Took %.5f seconds.\n\n", sec_local / 1000000);
        
    std::vector <weight_t> distances;
    
    std::cout << "Running Dijkstra ...\n";
    begin = std::chrono::steady_clock::now();
    DijkstraComputePaths(0, distances);
    end = std::chrono::steady_clock::now();
    sec_local = std ::chrono ::duration_cast <std ::chrono ::microseconds> (end - begin).count();
    printf("Dijkstra is done. Took %.3f seconds.\n\n", sec_local / 1000000);

    write_output(distances, sec_local);    

    delete [] iters;
    delete [] values;
    delete [] column_indexes;
    delete [] row_sizes_accum;

    return 0;
}
